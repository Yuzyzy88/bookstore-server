let button = document.getElementById('submit')

button.onclick = async (event) => {
    event.preventDefault()
    let error = document.getElementById("error");
    let errorPass = document.getElementById("errorPass");
    let pass = document.getElementById("password").value;
    let firstName = document.getElementById("inputName").value;
    let lastName = document.getElementById("inputLName").value;
    let email = document.getElementById("inputEmail3").value;
    let gender = Array.from(document.getElementsByName("gridRadios")).find(element => element.checked == true).value;
    let birthday = document.getElementById("birthday").value;
    let file = document.getElementById("file").value;
    let form = document.getElementById("form");

    if (lastName < 1) {
        error.innerHTML = "<span style='color:red;'>" + "Please enter last name</span>"
        return false;
    }

    if (pass == '') {
        errorPass.innerHTML = "<span style='color:red;'>" + "Please enter password</span>"
        return false;
    }

    if (!(pass.match(/[a-z]/g))) {
        errorPass.innerHTML = "<span style='color:red;'>" + "A lowercase letter</span>"
        return false;
    }

    if (!(pass.match(/[A-Z]/g))) {
        errorPass.innerHTML = "<span style='color:red;'>" + "A capital letter</span>"
        return false;
    }

    if (!(pass.match(/[0-9]/g))) {
        errorPass.innerHTML = "<span style='color:red;'>" + "A number</span>"
        return false;
    }

    if (pass.length < 8) {
        errorPass.innerHTML = "<span style='color:red;'>" + "Minimum 8 character</span>"
        return false;
    }

    else {
        form.innerHTML = `<p>Name : ${firstName}</p>
        <p>Last Name : ${lastName}</p>
        <p>Email : ${email}</p>
        <p>Pass : ${pass}</p>
        <p>Gender: ${gender}</p>
        <p>Birthday: ${birthday}</p>
        <p>File : ${file}</p>`;
    }
}