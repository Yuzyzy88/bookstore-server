## About 
Continuation of the [bookstore](https://gitlab.com/Yuzyzy88/challenge-topic4) project by implementing a server to display all pages

## Tech 
- [Express.js 4.17.3.](http://expressjs.com/).
- [Bootstrap v5.0](https://getbootstrap.com/)
